<h1>Package Enssop/enssop-Familyportal</h1>
<span>Pour intaller le paquet : </span>
<ol>
    <li>Faire un composer require enssop/familyportal</li> 
    <li>Pour un projet Laravel rajouter cette ligne dans le fichier config/app.php : Enssop\FamilyPortal\FamilyPortalServiceProvider::class,</li>

</ol>
