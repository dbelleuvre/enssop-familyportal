<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->unique()->nullable();
            $table->bigInteger('person_role_id')->unsigned();
            $table->bigInteger('civility_id')->unsigned();
            $table->bigInteger('family_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('birthdate')->nullable();
            $table->string('birthplace')->nullable();
            $table->bigInteger('situation_id')->unsigned()->nullable();
            $table->string('adress')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');
            $table->foreign('person_role_id')->references('id')->on('person_roles')->onDelete('cascade');
            $table->foreign('civility_id')->references('id')->on('civilities')->onDelete('cascade');
            $table->foreign('situation_id')->references('id')->on('situations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
