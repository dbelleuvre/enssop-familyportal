<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('person_id')->unsigned();
            $table->bigInteger('family_id')->unsigned();
            $table->bigInteger('activity_id')->unsigned();
            $table->bigInteger('school_id')->unsigned();
            //$table->bigInteger('tare_category_id')->unsigned();
            //$table->bigInteger('payer_account_id')->unsigned();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('status')->default("En Attente");
            $table->boolean('go_out')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');
            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');
            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            //$table->foreign('tare_category_id')->references('id')->on('tare_categories')->onDelete('cascade');
            //$table->foreign('payer_account_id')->references('id')->on('payer_accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
