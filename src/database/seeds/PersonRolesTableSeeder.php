<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\PersonRole;

class PersonRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person_roles = [
            [
                'title' => "Parent",
            ],
            [
                'title' => "Enfant",
            ],
            [
                'title' => "Contact",
            ],
        ];

        foreach ($person_roles as $person_role) {
            PersonRole::create($person_role);
        }
    }
}
