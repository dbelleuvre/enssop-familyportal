<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\School;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = [
            [   
                'title' => "Ecole maternelle du Grand Douai",
                'address' => "2 rue du Grand Douai",
                'city' => "Château-du-Loir",
                'postal_code' => "72500",
                'school_code' => "0720101U",
                'phone'=> "02.43.44.02.34",
            ],
            [
                'title' => "Ecole maternelle Laurentine Proust",
                'address' => "rue Laurentine Proust",
                'city' => "Château-du-Loir",
                'postal_code' => "72500",
                'school_code' => "0721400F",
                'phone'=> "02.43.44.05.37",
            ],
            [
                'title' => "Ecole primaire Beauregard",
                'address' => "allée de Bourgogne",
                'city' => "Château-du-Loir",
                'postal_code' => "72500",
                'school_code' => "0720100T",
                'phone'=> "02.43.44.02.17",
            ],
            [
                'title' => "Ecole primaire du Point du Jour",
                'address' => "rue du Haras",
                'city' => "Château-du-Loir",
                'postal_code' => "72500",
                'school_code' => "0720099S",
                'phone'=> "02.43.44.05.36",
            ],
            [
                'title' => "Ecole maternelle et primaire Robineau",
                'address' => "21 rue Oscar Moneris",
                'city' => "Château-du-Loir",
                'postal_code' => "72500",
                'school_code' => "0490610X",
                'phone'=> "02.43.79.45.38",
            ],
        ];

        foreach ($schools as $school) {
            School::create($school);
        }
    }
}
