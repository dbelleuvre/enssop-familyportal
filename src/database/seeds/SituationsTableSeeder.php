<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\Situation;

class SituationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $situations = [
            [
                'title' => "Marié(e)", 'title_M' => "Marié", 'title_F' => "Mariée",
            ],
            [
                'title' => "Séparé(e)", 'title_M' => "Séparé", 'title_F' => "Séparée",
            ],
            [
                'title' => "Remarié(e)", 'title_M' => "Remarié", 'title_F' => "Remariée",
            ],
            [
                'title' => "Célibataire", 'title_M' => "Célibataire", 'title_F' => "Célibataire",
            ],
            [
                'title' => "Divorcé(e)", 'title_M' => "Divorcé", 'title_F' => "Divorcée",
            ],
            [
                'title' => "Vie Maritale", 'title_M' => "Vie Maritale", 'title_F' => "Vie Maritale",
            ],
            [
                'title' => "Veuf(ve)", 'title_M' => "Veuf", 'title_F' => "Veufve",
            ],
            [
                'title' => "Pacsé(e)", 'title_M' => "Pacsé", 'title_F' => "Pacsée",
            ],
        ];

        foreach ($situations as $situation) {
            Situation::create($situation);
        }
    }
}
