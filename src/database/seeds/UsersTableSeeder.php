<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//use Enssop\FamilyPortal\Models\User;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =[
            [
                'name' => "dbelleuvre",
                'email' => "d.belleuvre@enssop.fr",
            ],
            [
                'name' => "gneveu",
                'email' => "g.neveu@enssop.fr",
            ],
            [
                'name' => "qvarin",
                'email' => "q.varin@enssop.fr",
            ],
        ];

        foreach ($users as $user) {
            $new_user = new User();
            $new_user->name = $user['name'];
            $new_user->email = $user['email'];
            $new_user->password = bcrypt($user['name']);
            $new_user->save();

            switch ($new_user->id) {
                case 2:
                    $new_user->assignRole('super-admin');
                break;
                case 1:
                    $new_user->assignRole('parent');
                break;
                case 3:
                    $new_user->assignRole('agent');
                break;
            }
        }
    }
}