<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FamilyPortalDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(UsersTableSeeder::class);
        $this->call(PersonRolesTableSeeder::class);
        $this->call(CivilitiesTableSeeder::class);
        $this->call(SituationsTableSeeder::class);
        $this->call(SchoolsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(PersonsTableSeeder::class);
    }
}
