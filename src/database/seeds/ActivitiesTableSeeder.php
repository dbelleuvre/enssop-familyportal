<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\Activity;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activities = [
            [
                'title' => "Scolaire",
            ],
            [
                'title' => "Restauration",
            ],
            [
                'title' => "Surveillé",
            ],
            [
                'title' => "Loisir",
            ]
        ];
        
        foreach ($activities as $activity) {
            Activity::create($activity);
        }
    }
}
