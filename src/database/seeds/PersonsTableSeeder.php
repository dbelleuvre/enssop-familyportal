<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\Person;
use Enssop\FamilyPortal\Models\Family;

class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $family = Family::create();
        $person = [
            'user_id' => 1,
            'person_role_id' => 1,
            'civility_id' => 1,
            'family_id' => $family->id,
            'first_name' => "David",
            'last_name' => "Belleuvre"
        ];
        Person::create($person);

        $family = Family::create();
        $person = [
            'user_id' => 2,
            'person_role_id' => 1,
            'civility_id' => 1,
            'family_id' => $family->id,
            'first_name' => "Gregor",
            'last_name' => "Neveu"
        ];
        Person::create($person);

        $family = Family::create();
        $person = [
            'user_id' => 3,
            'person_role_id' => 1,
            'civility_id' => 1,
            'family_id' => $family->id,
            'first_name' => "Quentin",
            'last_name' => "Varin"
        ];
        Person::create($person);
    }
}