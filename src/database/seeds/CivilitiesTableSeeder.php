<?php

namespace Enssop\FamilyPortal\Database\Seeds;

use Illuminate\Database\Seeder;

use Enssop\FamilyPortal\Models\Civility;

class CivilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $image_path = "enssop/FamilyPortal/avatar/";
        $civilities = [
            [
                'category' => "Adulte",
                'title' => "Monsieur",
                'short_name' => "M.",
                'image' => "Homme.png",
                'image_path' => $image_path."Homme.png",
            ],
            [
                'category' => "Adulte",
                'title' => "Mademoiselle",
                'short_name' => "Mlle",
                'image' => "Femme.png",
                'image_path' => $image_path."Femme.png",
            ],
            [
                'category' => "Adulte",
                'title' => "Madame",
                'short_name' => "Mme",
                'image' => "Femme.png",
                'image_path' => $image_path."Femme.png",

            ],
            [
                'category' => "Enfant",
                'title' => "Garçon",
                'short_name' => null,
                'image' => "Garcon.png",
                'image_path' => $image_path."Garcon.png",
            ],
            [
                'category' => "Enfant",
                'title' => "Fille",
                'short_name' => null,
                'image' => "Fille.png",
                'image_path' => $image_path."Fille.png",
            ],
        ];

        foreach ($civilities as $civility) {
            Civility::create($civility);
        }
    }
}