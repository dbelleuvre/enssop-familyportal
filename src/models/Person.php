<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\User;
use Enssop\FamilyPortal\Models\PersonRole;
use Enssop\FamilyPortal\Models\Registration;
use Enssop\FamilyPortal\Models\Authorization;

class Person extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the user that owns the person.
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the role that owns the person.
    */
    public function role()
    {
        return $this->belongsTo(PersonRole::class, 'person_role_id');
    }

    /**
     * Get the situation that owns the legal_guardian.
    */
    public function situation()
    {
        return $this->belongsTo(Situation::class);
    }

    /**
     * Get the civility that owns the legal_guardian.
    */
    public function civility()
    {
        return $this->belongsTo(Civility::class);
    }

    /**
     * Get the family that owns the person.
    */
    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    /**
     * Get the authorizations record associated with the person.
    */
    public function child_authorizations()
    {
        return $this->hasMany(Authorization::class, 'child_id');
    }

    /**
     * Get the authorizations record associated with the person.
    */
    public function other_authorizations()
    {
        return $this->hasMany(Authorization::class, 'other_id');
    }

    /**
     * Get the registrations record associated with the person.
    */
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }

    /**
     * Handle the legal_guardian "deleted" event.
    */
    public function delete()
    {
        $this->registrations()->delete();
        $this->child_authorizations()->delete();
        $this->other_authorizations()->delete();
        return parent::delete();
    }
}
