<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Person;
use Enssop\FamilyPortal\Models\Registration;

class School extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the people record associated with school.
     */
    public function people()
    {
        return $this->hasMany(Person::class);
    }

    /**
     * Get the registrations record associated with school.
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }
}
