<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Person;

class Civility extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the people record associated with the civility.
     */
    public function people()
    {
        return $this->hasMany(Person::class);
    }

}
