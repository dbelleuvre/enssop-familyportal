<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Person;

class Authorization extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

        /**
     * Get the child record associated with the OAuthorization.
     */
    public function child()
    {
        return $this->belongsTo(Person::class, 'child_id');
    }

    /**
     * Get the orther guardian record associated with the Authorization.
     */
    public function orther()
    {
        return $this->belongsTo(Person::class, 'other_id');
    }
}
