<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Registration;

class Activity extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the registrations record associated with the activity.
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }
}
