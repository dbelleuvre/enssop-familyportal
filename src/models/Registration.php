<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Person;
use Enssop\FamilyPortal\Models\Family;
use Enssop\FamilyPortal\Models\Activity;

class Registration extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the activity that owns the registration.
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Get the school that owns the registration.
    */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Get the person that owns the registration.
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * Get the family that owns the registration.
     */
    public function family()
    {
        return $this->belongsTo(Family::class);
    }
}
