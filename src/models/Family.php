<?php

namespace Enssop\FamilyPortal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Enssop\FamilyPortal\Models\Attachment;
use Enssop\FamilyPortal\Models\Registration;
use Enssop\FamilyPortal\Models\Person;

class Family extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the registration record associated with the family.
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }

    /**
     * Get the people that owns the family.
     */
    public function people()
    {
        return $this->hasMany(Person::class);
    }
}
