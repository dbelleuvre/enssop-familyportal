<?php

namespace Enssop\FamilyPortal\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'civility_id' => 'required|numeric',
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'birthdate' => 'nullable|Date',
            'birthplace' => 'nullable|string|25',
            'mobile_phone' => 'nullable|string',
            'email' => 'nullable|email|max:50'
        ];
    }
}
