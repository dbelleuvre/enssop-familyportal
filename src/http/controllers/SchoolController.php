<?php

namespace Enssop\FamilyPortal\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Enssop\FamilyPortal\Models\School;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('FamilyPortal::schools.index')->with('schools', School::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('FamilyPortal::schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        School::create($request->except('_token', '_method'));
        
        return redirect()->route('school.index')->withStatus('École crée avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        return view('FamilyPortal::schools.show')->with('school', $school);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        return view('FamilyPortal::schools.edit')->with('school', $school);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        $school->update($request->except('_token', '_method'));
        return redirect()->route('school.index')->withStatus('École modifié avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        $school->delete();
        return redirect()->route('school.index')->withStatus('École supprimé avec succès.');
    }
}