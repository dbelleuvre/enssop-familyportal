<?php

namespace Enssop\FamilyPortal\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Enssop\FamilyPortal\Models\User;
use Enssop\FamilyPortal\Models\Person;
use Enssop\FamilyPortal\Models\Civility;
use Enssop\FamilyPortal\Models\Situation;
use Enssop\FamilyPortal\Models\Authorization;


class PersonController extends Controller
{
    private $family;    // Variable famille de utilisateur
    private $children;    // Variable enfants de utilisateur

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            $userId = \Auth::id();
            $this->family = (!empty(User::find($userId)->person->first())) ? 
            User::find($userId)->person->family->first() : null ;
            $this->children = $this->family->people->where('person_role_id', 2)->all();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('FamilyPortal::person.index')
        ->with('people', Person::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $Request)
    {
        $civilities = ($Request->person_role_id == 2) ? 
        Civility::where('category', "Enfant")->get() : 
        Civility::where('category', "Adulte")->get() ;  // civilite fonction de enfant ou adulte
        $col = ($Request->person_role_id == 3) ? 4 : 6 ;    // col page create

        return view('FamilyPortal::person.create',[
            'person_role_id' => $Request->person_role_id,   // Role de la personne
            'civilities' => $civilities,   // Civilités d'une personne
            'situations' => Situation::all(), // Situations d'une personne
            'family_id' => $this->family->id, // Id Famille
            'children' => $this->children,  // Enfant de la famille
            'col' => $col   // col page
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->person_role_id == 3) {
            $arguments = [
                'user_id',
                'person_role_id',
                'civility_id',
                'family_id',
                'first_name',
                'last_name',
                'birthdate',
                'birthplace',
                'situation_id',
                'adress',
                'postalcode',
                'city',
                'phone',
                'mobile_phone',
                'email'
            ];
            $requestData = $request->except('_token', '_method');
            $requestPerson = Arr::only($requestData, $arguments);
            $person = Person::create($requestPerson);

            foreach ($this->children as $child) {
                $requestAuthorization = ['child_id' => $child->id, 'other_id' => $person->id];  // Tableau des autorisations
                $emergency = ($request->has('emergency_'.$child->id)) ? $request->input('emergency_'.$child->id) : null ;   // // valeur de input 'emergency_{}'
                $requestAuthorization = Arr::add($requestAuthorization, 'emergency', $emergency);    // Ajoute 'emergency' a autorisations
                $exit = ($request->has('exit_'.$child->id)) ? $request->input('exit_'.$child->id) : null ;    // valeur de input 'exit_{}'
                $requestAuthorization = Arr::add($requestAuthorization, 'exit', $exit);   // Ajoute 'exit' a autorisations
                Authorization::create($requestAuthorization);  // Crée les autorisations  
            }

        } else {
            $person = Person::create($request->except('_token', '_method'));
        }

        return redirect()->route('FamilyPortal')->withStatus($person->first_name.' '.$person->last_name.' crée avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  Person $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        $civlities = ($person->person_role_id == 2) ?
        Civility::where('category', "Enfant")->get() : 
        Civility::where('category', "Adulte")->get() ;
        $col = ($person->role->id == 3 ) ? 4 : 6 ;

        return view('FamilyPortal::person.show',[
            'civilities' => $civlities,   // Civilités d'une personne
            'situations' => Situation::all(), // Situations d'une personne
            'children' => $this->children,  // Enfant de la famille
            'col' => $col    //col page
            ]
        )->with('person', $person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Person $person
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        $civlities = ($person->person_role_id == 2) ?
        Civility::where('category', "Enfant")->get() : 
        Civility::where('category', "Adulte")->get() ;
        $col = ($person->role->id == 3 ) ? 4 : 6 ;

        return view('FamilyPortal::person.edit',[
            'civilities' => $civlities,   // Civilités d'une personne
            'situations' => Situation::all(), // Situations d'une personne
            'children' => $this->children,  // Enfant de la famille
            'col' => $col   // col page 
            ]
        )->with('person', $person);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Person $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        if ($person->role->id == 3) {
            $arguments = [
                'user_id',
                'person_role_id',
                'civility_id',
                'family_id',
                'first_name',
                'last_name',
                'birthdate',
                'birthplace',
                'situation_id',
                'adress',
                'postalcode',
                'city',
                'phone',
                'mobile_phone',
                'email'
            ];
            $requestData = $request->except('_token', '_method');
            $requestPerson = Arr::only($requestData, $arguments);
            $person->update($requestPerson);

            foreach ($this->children as $child) {
                $requestAuthorization = ['child_id' => $child->id, 'other_id' => $person->id];  // Tableau des autorisations
                $emergency = ($request->has('emergency_'.$child->id)) ? $request->input('emergency_'.$child->id) : null ;   // // valeur de input 'emergency_{}'
                $requestAuthorization = Arr::add($requestAuthorization, 'emergency', $emergency);    // Ajoute 'emergency' a autorisations
                $exit = ($request->has('exit_'.$child->id)) ? $request->input('exit_'.$child->id) : null ;    // valeur de input 'exit_{}'
                $requestAuthorization = Arr::add($requestAuthorization, 'exit', $exit);   // Ajoute 'exit' a autorisations
                if ($person->other_authorizations->where('child_id', $child->id)->first()) {
                    $person->other_authorizations->where('child_id', $child->id)->first()->update($requestAuthorization);  // Mise a jour les autorisations
                } else {
                    Authorization::create($requestAuthorization);  // Crée les nouvelles autorisations 
                }
                  
            }

        } else {
            $person->update($request->except('_token', '_method'));
        }

        return redirect()->route('FamilyPortal')->withStatus($person->first_name.' '.$person->last_name.' modifié avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Person $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        $person->delete();
        return redirect()->route('FamilyPortal')->withStatus($person->first_name.' '.$person->last_name.' supprimé avec succès.');
    }
}