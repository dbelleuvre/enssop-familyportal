<?php

namespace Enssop\FamilyPortal\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Enssop\FamilyPortal\Models\User;
use Enssop\FamilyPortal\Models\Person;

class FamilyPortalController extends Controller
{
    /**
     * Show the application dashboard
     * 
     * @return Response
     */
    public function index()
    {
        $user = User::find(Auth::id())->first();

        switch (Auth::user()->roles->first()->name) {
            case 'agent':
            return view('FamilyPortal::agent')->with([
                'user' => $user,
            ]);
                break;
            case 'parent':
            $resp = $user->person;    // Responsable de famille
            $family = $user->person->family;    // Famille
            $co_resp = $family->people->where('user_id', null)->where('person_role_id', 1)->first();   // Co-Responsable de la famille
            $children = $family->people->where('person_role_id', 2)->all();   // Enfants de la famille
            $other_resps = $family->people->where('person_role_id', 3)->all(); // Contact de la famille
            
            return view('FamilyPortal::index')->with([
                'resp' => $resp,
                'co_resp' => $co_resp,
                'children' => $children,
                'other_resps' => $other_resps,
            ]);
                break;
            default:
                return back();
                break;
        }
    }
}