<?php

namespace Enssop\FamilyPortal\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Enssop\FamilyPortal\Models\User;
use Enssop\FamilyPortal\Models\School;
use Enssop\FamilyPortal\Models\Registration;
use Enssop\FamilyPortal\Models\Activity;

class RegistrationController extends Controller
{
    private $family;    // Variable famille de utilisateur
    private $children;    // Variable enfants de utilisateur

    public function __construct()
    {
        $this->middleware(function (Request $request, $next) {
            $userId = \Auth::id();
            $this->family = (!empty(User::find($userId)->person->first())) ? 
            User::find($userId)->person->family->first() : null ;
            $this->children = $this->family->people->where('person_role_id', 2)->all();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        switch (Auth::user()->roles->first()->name) {   // Role de uilisateur
            case 'parent':
                $family = User::find(Auth::id())->person->family;   // Famille de utilisateur
                $registration = Registration::where('family_id', $family->id)->get();   // Inscription de la famille
                return view('FamilyPortal::registrations.index')->with('registrations', $registration);
                break;
            case 'agent':
            switch ($request->status) {
                case 'attente':
                    $registration = Registration::where('status', 'En Attente')->get();
                    return view('FamilyPortal::registrations.index')->with('registrations', $registration);
                    break;
                default:
                    return view('FamilyPortal::registrations.index')->with('registrations', Registration::all());
                    break;
            }
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('FamilyPortal::registrations.create', [
            'children' => $this->children,
            'activities' => Activity::all(),
            'schools' => School::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request = $request->except('_token', '_method');
        $request = Arr::add($request, 'family_id', $this->family->id);
        Registration::create($request);

        return redirect()->route('registration.index')->withStatus('Inscription crée avec succès.');
    }

    /**
     * Show the specified resource.
     * @param  Registration $registration
     * @return Response
     */
    public function show(Registration $registration)
    {
        return view('FamilyPortal::registrations.show', [
            'children' => $this->children,
            'activities' => Activity::all(),
            'schools' => School::all(),
        ])->with('registration', $registration);
    }

    /**
     * Show the form for editing the specified resource.
     * @param  Registration $registration
     * @return Response
     */
    public function edit(Registration $registration)
    {
        return view('FamilyPortal::registrations.edit', [
            'children' => $this->children,
            'activities' => Activity::all(),
            'schools' => School::all(),
        ])->with('registration', $registration);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param  Registration $registration
     * @return Response
     */
    public function update(Request $request, Registration $registration)
    {
        $registration->update($request->except('_token', '_method'));
        
        return redirect()->route('registration.index')->withStatus('Inscription modifié avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     * @param  Registration $registration
     * @return Response
     */
    public function destroy(Registration $registration)
    {
        $registration->delete();
        return redirect()->route('registration.index')->withStatus('Inscription supprimé avec succès.');
    }
}