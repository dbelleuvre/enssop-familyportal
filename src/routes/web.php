<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::prefix('familyportal')->group(function() {
        Route::get('/', 'Enssop\FamilyPortal\Http\Controllers\FamilyPortalController@index')->name('FamilyPortal');
        Route::resource('/user', 'Enssop\FamilyPortal\Http\Controllers\UserController');
        Route::resource('/person', 'Enssop\FamilyPortal\Http\Controllers\PersonController');
        Route::resource('/registration', 'Enssop\FamilyPortal\Http\Controllers\RegistrationController');
        Route::prefix('agent')->group(function() {
        Route::get('/', 'Enssop\FamilyPortal\Http\Controllers\AgentController@index')->name('Agent');
        Route::resource('/school', 'Enssop\FamilyPortal\Http\Controllers\SchoolController');
        });
        Route::fallback(function () {
            return redirect()->route('/home');
        });
    });
});
