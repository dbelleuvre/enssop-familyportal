<?php

namespace Enssop\FamilyPortal;

use Illuminate\Support\ServiceProvider;

class FamilyPortalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Controllers
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\PersonController');
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\AgentController');
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\FamilyPortalController');
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\RegistrationController');
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\UserController');
        $this->app->make('Enssop\FamilyPortal\Http\Controllers\SchoolController');
        // Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'FamilyPortal');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router'];
        // Routes
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        // Migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        // Translations
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'FamilyPortal');
        // Public Assets
        $this->publishes([
            __DIR__.'/resources/asset' => public_path('enssop/FamilyPortal'),
        ], 'public');
    }
}
