<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- dataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

    <title>{!! config('FamilyPortal.name') !!}</title>

    <!-- CSS File -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('enssop/FamilyPortal/css/FamilyPortal.css') }}">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <a class="navbar-brand" href="{{ route('FamilyPortal') }}">
                    {{ preg_replace('/(?=(?<!^)[[:upper:]])/', ' ', config('FamilyPortal.name', 'Portail Famille')) }}
                </a>
                @role('parent')
                <div class="dropdown">
                    <a class="navbar-brand" data-toggle="dropdown">Ajouter un menbre à la famille</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('person.create', ['person_role_id' => 1]) }}">Parent</a>
                        <a class="dropdown-item" href="{{ route('person.create', ['person_role_id' => 2]) }}">Enfant</a>
                        <a class="dropdown-item" href="{{ route('person.create', ['person_role_id' => 3]) }}">Contact</a>
                    </div>
                </div>
                @endrole
                <a class="navbar-brand" href="#">Actualités</a>
                <a class="navbar-brand" href="#">Activités</a>
                <a class="navbar-brand" href="#">Infos Pratiques</a>
                <a class="navbar-brand" href="#">Edition de documents</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main id="app" class="py-4">
            @yield('content')
        </main>
    </div>
    {{-- Laravel JS File --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" defer></script>

    <!-- dataTables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>

    {{-- dataTables config --}}
    <script src="{{ asset('enssop/FamilyPortal/js/FamilyPortal-Datatable.js') }}" defer></script>

    {{-- JS File --}}
    <script src="{{ asset('enssop/FamilyPortal/js/FamilyPortal.js') }}" defer></script>

</body>

</html>