@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">

            </div>
            <h4 class="card-title">
                Fiche d'inscription
            </h4>
            <p class="card-category"></p>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label for="registration_person" class="col-2 col-form-label"><b>Enfant : </b></label>
                <div class="col-10">
                    <select class="form-control" id="registration_person" name="person_id">
                        <option value="" disabled>Sélectionnez un enfant...</option>
                        @foreach ($children as $child)
                        <option @if ($child->id == $registration->person->id) selected disabled @endif
                            value="{{ $child->id }}" disabled>{{ $child->first_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="registration_$activity" class="col-2 col-form-label"><b>Activité : </b></label>
                <div class="col-10">
                    <select class="form-control" id="registration_activity" name="activity_id">
                        <option value="" disabled>Sélectionnez une activité...</option>
                        @foreach ($activities as $activity)
                        <option @if ($activity->id == $registration->activity->id) selected @endif
                            value="{{ $activity->id }}" disabled>{{ $activity->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="registration_school" class="col-2 col-form-label"><b>Ecole : </b></label>
                <div class="col-10">
                    <select class="form-control" id="registration_school" name="school_id">
                        <option value="" disabled>Sélectionnez une école..</option>
                        @foreach ($schools as $school)
                        <option @if ($school->id == $registration->school->id) selected @endif
                            value="{{ $school->id }}" disabled>{{ $school->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="registration_start_date" class="col-2 col-form-label"><b>Date de début : </b></label>
                <div class="col-10">
                    <input class="form-control" type="date" id="registration_start_date" name="start_date"
                        value="{{ $registration->start_date }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="registration_end_date" class="col-2 col-form-label"><b>Date de fin : </b></label>
                <div class="col-10">
                    <input class="form-control" type="date" id="registration_end_date" name="end_date"
                        value="{{ $registration->end_date }}" disabled>
                </div>
            </div>
        </div>
        <div class="my-3 mx-auto">
            <div class="row">
                <div class="col-6">
                    <form action="{{ route('registration.edit', $registration) }}" method="POST">
                        @csrf
                        @method('GET')
                        <button type="submit" class="btn btn-secondary">Editer</button>
                    </form>
                </div>
                <div class="col-6">
                    <form action="{{ route('registration.destroy', $registration) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button onclick="return confirm('Êtes-vous sûr de vouloir supprimer l\'inscription ?')" type="submit" class="btn btn-danger">Suprimer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection