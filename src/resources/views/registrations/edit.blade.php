@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <form class="my-5" action="{{ route('registration.update', $registration ) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">

                </div>
                <h4 class="card-title">
                    Editer une inscription
                </h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="registration_person" class="col-2 col-form-label"><b>Enfant : </b></label>
                    <div class="col-10">
                        <select class="form-control" id="registration_person" name="person_id">
                            <option value="" disabled>Sélectionnez un enfant...</option>
                            @foreach ($children as $child)
                            <option @if ($child->id == $registration->person->id) selected @endif value="{{ $child->id }}">{{ $child->first_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="registration_$activity" class="col-2 col-form-label"><b>Activité : </b></label>
                    <div class="col-10">
                        <select class="form-control" id="registration_activity" name="activity_id">
                            <option value="" disabled>Sélectionnez une activité...</option>
                            @foreach ($activities as $activity)
                            <option @if ($activity->id == $registration->activity->id) selected @endif value="{{ $activity->id }}">{{ $activity->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="registration_school" class="col-2 col-form-label"><b>Ecole : </b></label>
                    <div class="col-10">
                        <select class="form-control" id="registration_school" name="school_id">
                            <option value="" disabled>Sélectionnez une école..</option>
                            @foreach ($schools as $school)
                            <option @if ($school->id == $registration->school->id) selected @endif value="{{ $school->id }}">{{ $school->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="registration_start_date" class="col-2 col-form-label"><b>Date de début : </b></label>
                    <div class="col-10">
                        <input class="form-control" type="date" id="registration_start_date" name="start_date" value="{{ $registration->start_date }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="registration_end_date" class="col-2 col-form-label"><b>Date de fin : </b></label>
                    <div class="col-10">
                    <input class="form-control" type="date" id="registration_end_date" name="end_date" value="{{ $registration->end_date }}">
                    </div>
                </div>
            </div>
            <div class="my-3 mx-auto">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </form>
</div>
@endsection