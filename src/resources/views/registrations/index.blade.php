@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="card-icon">
                <i class="fas fa-list"></i>
            </div>
            <h4 class="card-title">
                Liste des Inscriptions
            </h4>
            <p class="card-category"></p>
        </div>
        <div class="card-body">
            @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                    </div>
                </div>
            </div>
            @endif
            <table class="datatable">
                <thead class="thead-dark">
                    <tr>
                        <th>Enfant</th>
                        <th>Activité</th>
                        <th>Ecole</th>
                        <th>Date de inscription</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Status</th>
                        <th>Parti</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($registrations as $registration)
                    <tr>
                        <td>{{ $registration->person->first_name }}</td>
                        <td>{{ $registration->activity->title }}</td>
                        <td>{{ $registration->school->title }}</td>
                        <td>{{ date('d-m-Y', strtotime($registration->created_at)) }}</td>
                        <td>{{ date('d-m-Y', strtotime($registration->start_date)) }}</td>
                        <td>{{ date('d-m-Y', strtotime($registration->end_date)) }}</td>
                        <td>{{ $registration->status }}</td>
                        <td>{{ $registration->go_out }}</td>
                        <td>
                            <form class="d-flex justify-content-center"
                                action="{{ route('registration.show', $registration) }}" method="POST">
                                @csrf
                                @method('GET')
                                <button type="submit" class="btn btn-info"><i class="far fa-eye"></i></button>
                            </form>
                        </td>
                        <td>
                            <form class="d-flex justify-content-center"
                                action="{{ route('registration.edit', $registration) }}" method="POST">
                                @csrf
                                @method('GET')
                                <button type="submit" class="btn btn-warning"><i class="far fa-edit"></i></button>
                            </form>
                        </td>
                        <td>
                            <form class="d-flex justify-content-center"
                                action="{{ route('registration.destroy', $registration) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button onclick="return confirm('Êtes-vous sûr de vouloir supprimer l\'inscription ?')"
                                    type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection