@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
        <table class="datatable">
            <thead>
                <tr>
                    <th>Famille</th>
                    <th>Civilité</th>
                    <th>Role</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Né le</th>
                    <th>A</th>
                    <th>Situation</th>
                    <th>Adresse</th>
                    <th>Code postal</th>
                    <th>Ville</th>
                    <th>Téléphone Domicile</th>
                    <th>Portable</th>
                    <th>Email</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($people as $person)
                <tr>
                    <td>{{ $person->family->id }}</td>
                    <td>{{ $person->civility->title }}</td>
                    <td>{{ $person->role->title }}</td>
                    <td>{{ $person->last_name }}</td>
                    <td>{{ $person->first_name }}</td>
                    <td>{{ $person->birthdate }}</td>
                    <td>{{ $person->birthplace }}</td>
                    @if ($person->situation_id == null)
                    <td></td>
                    @else
                    <td>{{ $person->situation->title }}</td>
                    @endif
                    <td>{{ $person->adress }}</td>
                    <td>{{ $person->postalcode }}</td>
                    <td>{{ $person->city }}</td>
                    <td>{{ $person->phone }}</td>
                    <td>{{ $person->mobile_phone }}</td>
                    <td>{{ $person->email }}</td>
                    <td>
                        <form class="d-flex justify-content-center" action="{{ route('person.show', $person) }}"
                            method="POST">
                            @csrf
                            @method('GET')
                            <button type="submit" class="btn btn-info"><i class="far fa-eye"></i></button>
                        </form>
                    </td>
                    <td>
                        <form class="d-flex justify-content-center" action="{{ route('person.edit', $person) }}"
                            method="POST">
                            @csrf
                            @method('GET')
                            <button type="submit" class="btn btn-warning"><i class="far fa-edit"></i></button>
                        </form>
                    </td>
                    <td>
                        <form class="d-flex justify-content-center" action="{{ route('person.destroy', $person) }}"
                            method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection