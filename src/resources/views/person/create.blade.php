@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <form class="my-5" action="{{ route('person.store') }}" method="POST">
        @csrf
        @method('POST')
        <input name="person_role_id" type="hidden" value="{{$person_role_id}}">
        <input name="family_id" type="hidden" value="{{$family_id}}">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    @switch($person_role_id)
                    @case(1)
                    Ajouter un Parent
                    @break
                    @case(2)
                    Ajouter un Enfant
                    @break
                    @case(3)
                    Ajouter un Conctat
                    @break
                    @default
                    Ajouter une Personne
                    @endswitch
                </h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-{{ $col }}">
                        <div class="card">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                </div>
                                <h4 class="card-title text-center">
                                    Identité
                                </h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-4 col-form-label"><b>Civilité :</b></label>
                                    <div class="col-8">
                                        @foreach ($civilities as $civility)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="civility_id"
                                                id="civility_{{ $civility->id }}" value="{{ $civility->id }}">
                                            <label class="form-check-label" for="civility_{{ $civility->id }}">
                                                @if ( $civility->short_name == null )
                                                {{ $civility->title }}
                                                @else
                                                {{ $civility->short_name }}
                                                @endif
                                            </label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-4 col-form-label"><b>Nom :</b></label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="last_name" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="first_name" class="col-4 col-form-label"><b>Prénom :</b></label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="first_name" name="first_name">
                                    </div>
                                </div>
                                @if ($person_role_id == 1)
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">Situation :</label>
                                    <div class="col-8">
                                        @foreach ($situations as $situation)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="situation_id"
                                                id="situation_{{ $situation->id }}" value="{{ $situation->id }}">
                                            <label class="form-check-label"
                                                for="situation_{{ $situation->id }}">{{ $situation->title }}
                                            </label>
                                        </div>
                                        @endforeach
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="situation_id"
                                                id="situation_0" value="">
                                            <label class="form-check-label" for="situation_0">Non renseigné</label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @role('parent')
                    <div class="col-md-{{ $col }}">
                        <div class="card">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                </div>
                                <h4 class="card-title text-center">
                                    Coordonnées
                                </h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body">
                                @if ($person_role_id == 1 || $person_role_id == 3)
                                <div class="form-group row">
                                    <label for="adress" class="col-4 col-form-label">Adresse :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="adress" name="adress">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="postalcode" class="col-4 col-form-label">Code Postal :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="postalcode" name="postalcode">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="city" class="col-4 col-form-label">Ville :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="city" name="city">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-4 col-form-label">Téléphone :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="phone" name="phone">
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <label for="mobile_phone" class="col-4 col-form-label">Portable :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" id="mobile_phone" name="mobile_phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Courriel :</label>
                                    <div class="col-8">
                                        <input class="form-control" type="email" id="email" name="email">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($person_role_id == 3)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                </div>
                                <h4 class="card-title text-center">
                                    Autorisations
                                </h4>
                                <p style="color:red" class="card-category">Précisez ci-dessous si cette personne doit
                                    être
                                    contactée en
                                    cas d'urgence
                                    et/ou si
                                    elle est autorisée à prendre l'enfant à la sortie.</p>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Enfant
                                            </th>
                                            <th class="text-center">
                                                Urgence
                                            </th>
                                            <th class="text-center">
                                                Sortie
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($children as $child)
                                        <tr>
                                            <th>
                                                {{ $child->first_name }}
                                            </th>
                                            <td class="text-center">
                                                <input type="checkbox" name="emergency_{{ $child->id }}" value="1">
                                            </td>
                                            <td class="text-center">
                                                <input class="text-center" type="checkbox" name="exit_{{ $child->id }}"
                                                    value="1">
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endrole
                </div>
                <div class="my-4 text-center">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection