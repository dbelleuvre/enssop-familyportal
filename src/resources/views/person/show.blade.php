@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
            </div>
            <h4 class="card-title">
                @switch($person->role->id)
                @case(1)
                Fiche du Parent
                @break
                @case(2)
                Fiche de l'Enfant
                @break
                @case(3)
                Fiche du Conctat
                @break
                @endswitch
            </h4>
            <p class="card-category"></p>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-{{ $col }}">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                            </div>
                            <h4 class="card-title text-center">
                                Identité
                            </h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body">
                            <label class="col-4 col-form-label"><b>Civilité :</b></label>
                            <div class="col-8">
                                @foreach ($civilities as $civility)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="civility_id"
                                        id="civility_{{ $civility->id }}" value="{{ $civility->id }}"
                                        @if($person->civility_id
                                    == $civility->id) checked @endif disabled>
                                    <label class="form-check-label" for="civility_{{ $civility->id }}">
                                        @if ( $civility->short_name == null )
                                        {{ $civility->title }}
                                        @else
                                        {{ $civility->short_name }}
                                        @endif
                                    </label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-4 col-form-label"><b>Nom :</b></label>
                            <div class="col-8">
                                <input class="form-control" type="text" id="last_name" name="last_name"
                                    value="{{ $person->last_name}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="first_name" class="col-4 col-form-label"><b>Prénom :</b></label>
                            <div class="col-8">
                                <input class="form-control" type="text" id="first_name" name="first_name"
                                    value="{{$person->first_name}}" disabled>
                            </div>
                        </div>
                        @if ($person->person_role_id == 1)
                        <div class="form-group row">
                            <label class="col-4 col-form-label">Situation :</label>
                            <div class="col-8">
                                @foreach ($situations as $situation)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="situation_id"
                                        id="situation_{{ $situation->id }}" value="{{ $situation->id }}"
                                        @if($person->situation_id == $situation->id) checked @endif disabled>
                                    <label class="form-check-label"
                                        for="situation_{{ $situation->id }}">{{ $situation->title }}
                                    </label>
                                </div>
                                @endforeach
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="situation_id" id="situation_0"
                                        value="" @if($person->situation_id == null) checked @endif
                                    disabled>
                                    <label class="form-check-label" for="situation_0">Non renseigné</label>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-{{ $col }}">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                            </div>
                            <h4 class="card-title text-center">
                                Coordonnées
                            </h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body">
                            @if ($person->person_role_id == 1 || $person->person_role_id == 3)
                            <div class="form-group row">
                                <label for="adress" class="col-4 col-form-label">Adresse :</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="adress" name="adress"
                                        value="{{$person->adress}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="postalcode" class="col-4 col-form-label">Code Postal :</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="postalcode" name="postalcode"
                                        value="{{$person->postalcode}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-4 col-form-label">Ville :</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="city" name="city"
                                        value="{{$person->city}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-4 col-form-label">Téléphone :</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="phone" name="phone"
                                        value="{{$person->phone}}" disabled>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                <label for="mobile_phone" class="col-4 col-form-label">Portable :</label>
                                <div class="col-8">
                                    <input class="form-control" type="text" id="mobile_phone" name="mobile_phone"
                                        value="{{$person->mobile_phone}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-4 col-form-label">Courriel :</label>
                                <div class="col-8">
                                    <input class="form-control" type="email" id="email" name="email"
                                        value="{{$person->email}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($person->role->id == 3)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                            </div>
                            <h4 class="card-title text-center">
                                Autorisations
                            </h4>
                            <p style="color:red" class="card-category">Précisez ci-dessous si cette personne doit
                                être
                                contactée en
                                cas d'urgence
                                et/ou si
                                elle est autorisée à prendre l'enfant à la sortie.</p>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            Enfant
                                        </th>
                                        <th class="text-center">
                                            Urgence
                                        </th>
                                        <th class="text-center">
                                            Sortie
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($children as $child)
                                    <tr>
                                        <th>
                                            {{ $child->first_name }}
                                        </th>
                                        <td class="text-center">
                                            <input type="checkbox" name="emergency_{{ $child->id }}" value="1"
                                                @if(isset($person->other_authorizations->where('child_id',
                                            $child->id)->first()->emergency)) checked @endif disabled>
                                        </td>
                                        <td class="text-center">
                                            <input class="text-center" type="checkbox" name="exit_{{ $child->id }}"
                                                value="1" @if(isset($person->other_authorizations->where('child_id',
                                            $child->id)->first()->exit)) checked @endif disabled>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="my-3 text-center">
                <div class="row">
                    <div class="col-6">
                        <form action="{{ route('person.edit', $person) }}" method="POST">
                            @csrf
                            @method('GET')
                            <button type="submit" class="btn btn-secondary">Editer</button>
                        </form>
                    </div>
                    <div class="col-6">
                        <form action="{{ route('person.destroy', $person) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button onclick="return confirm('Êtes-vous sûr de vouloir supprimer {{ $person->last_name }} {{ $person->first_name }} ?')" type="submit" class="btn btn-danger">Suprimer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection