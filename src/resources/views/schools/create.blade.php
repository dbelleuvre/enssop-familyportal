@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <form class="my-5" action="{{ route('school.store') }}" method="POST">
        @csrf
        @method('POST')
        <div class="card">
            <div class="card-header bg-primary">
                <h4 class="card-title">
                    Ajouter une école
                </h4>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="school_title" class="col-2 col-form-label">Nom : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_title" name="title">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="school_code" class="col-2 col-form-label">Numéro : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_code" name="school_code">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="school_address" class="col-2 col-form-label">Adresse : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_address" name="address">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="school_city" class="col-2 col-form-label">Ville : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_city" name="city">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="school_postal_code" class="col-2 col-form-label">Code Postal : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_postal_code" name="postal_code">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="school_phone" class="col-2 col-form-label">Téléphone : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="school_phone" name="phone">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="www_addresse" class="col-2 col-form-label">Site web : </label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="www_address" name="www_address">
                    </div>
                </div>
            </div>
            <div class="my-3 mx-auto">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </form>
</div>
@endsection