@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header bg-primary">
            <h4 class="card-title">
                Fiche de l'école
            </h4>
            <p class="card-category">
                <div class="text-right">
                    <a href="{{ route('school.index') }}" class="btn btn-sm btn-secondary">Retour liste</a>
                </div>
            </p>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label for="school_title" class="col-2 col-form-label">Nom : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_title" name="title" value="{{ $school->title }}"
                        disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="school_code" class="col-2 col-form-label">Numéro : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_code" name="school_code"
                        value="{{ $school->school_code }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="school_address" class="col-2 col-form-label">Adresse : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_address" name="address"
                        value="{{ $school->address }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="school_city" class="col-2 col-form-label">Ville : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_city" name="city" value="{{ $school->city }}"
                        disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="school_postal_code" class="col-2 col-form-label">Code Postal : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_postal_code" name="postal_code"
                        value="{{ $school->postal_code }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="school_phone" class="col-2 col-form-label">Téléphone : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="school_phone" name="phone" value="{{ $school->phone }}"
                        disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="www_addresse" class="col-2 col-form-label">Site web : </label>
                <div class="col-10">
                    <input class="form-control" type="text" id="www_address" name="www_address"
                        value="{{ $school->www_address }}" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection