@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header bg-primary">
            <h4 class="card-title">
                Liste des écoles
            </h4>
            <p class="card-category">
                <div class="text-right">
                    <a href="{{ route('school.create') }}" class="btn btn-sm btn-secondary">Ajouter une école</a>
                </div>
            </p>
        </div>
        <div class="card-body">
            @if (session('status'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-times"></i>
                        </button>
                        <span>{{ session('status') }}</span>
                    </div>
                </div>
            </div>
            @endif
            <table class="datatable">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Numéro</th>
                        <th>Adresse</th>
                        <th>Ville</th>
                        <th>Code Postal</th>
                        <th>Téléphone</th>
                        <th>Site</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($schools as $school)
                    <tr>
                        <td>{{ $school->title }}</td>
                        <td>{{ $school->school_code }}</td>
                        <td>{{ $school->address }}</td>
                        <td>{{ $school->city }}</td>
                        <td>{{ $school->postal_code }}</td>
                        <td>{{ $school->phone }}</td>
                        <td>{{ $school->www_address }}</td>
                        <td>
                            <form class="d-flex justify-content-center" action="{{ route('school.show', $school) }}"
                                method="POST">
                                @csrf
                                @method('GET')
                                <button type="submit" class="btn btn-info"><i class="far fa-eye"></i></button>
                            </form>
                        </td>
                        <td>
                            <form class="d-flex justify-content-center" action="{{ route('school.edit', $school) }}"
                                method="POST">
                                @csrf
                                @method('GET')
                                <button type="submit" class="btn btn-warning"><i class="far fa-edit"></i></button>
                            </form>
                        </td>
                        <td>
                            <form class="d-flex justify-content-center" action="{{ route('school.destroy', $school) }}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <button onclick="return confirm('Êtes-vous sûr de vouloir supprimer l\'école ?')"
                                    type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection