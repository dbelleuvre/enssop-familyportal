@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container-fluid">
    @if (session('status'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i>close</i>
                </button>
                <span>{{ session('status') }}</span>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div>
                <h2>MON ESPACE</h2>
                <div class="card col-md-8">
                    <img src="{{ asset($resp->civility->image_path) }}" class="card-img-top mx-auto avatar"
                        alt="{{ $resp->civility->image_path }}">
                    <div class="card-body text-center">
                        <h5 class="card-title">
                            {{ $resp->first_name }} <b>{{ $resp->last_name }}</b>
                        </h5>
                        <p class="card-text d-inline-block text-left">
                            {{ $resp->adress  }}<br>
                            {{ $resp->postalcode }} {{ $resp->city }}<br>
                            {{ $resp->phone }}<br>
                            {{ $resp->mobile_phone }}<br>
                            {{ $resp->email }}
                        </p>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    <a href="{{ route('person.show', $resp) }}">
                                        <i class="fas fa-user-alt">
                                            <p class="icon-profil">Profil</p>
                                        </i>
                                    </a>
                                </div>
                                <div class="col-6">
                                    <a href="{{ route('user.show', $resp->user->id)}}">
                                        <i class="fas fa-lock">
                                            <p class="icon-profil">Identifiants</p>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h2>CO-REPRESENTANTS</h2>
                @if ($co_resp !== null)
                <div class="card col-md-6" onclick="window.location='{{ route('person.show', $co_resp) }}'">
                    <img src="{{ asset($co_resp->civility->image_path) }}" class="card-img-top mx-auto avatar"
                        alt="{{ $co_resp->civility->image_path }}">
                    <div class="card-body">
                        <h5 class="card-title text-center">
                            {{ $co_resp->first_name }} <b>{{ $co_resp->last_name }}</b>
                        </h5>
                        <p class="card-text">

                        </p>
                    </div>
                </div>
                @endif
            </div>
            <div>
                <h2>MEMBRES FOYER</h2>
                <div class="card-deck">
                    @foreach ($children as $child)
                    <div class="card col-md-4" onclick="window.location='{{ route('person.show', $child) }}'">
                        <img src="{{ asset($child->civility->image_path) }}" class="card-img-top mx-auto avatar"
                            alt="{{ $child->civility->image_path }}">
                        <div class="card-body">
                            <h5 class="card-title text-center">
                                {{ $child->first_name }}
                            </h5>
                            <p class="card-text">

                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div>
                <h2>CONTACTS</h2>
                <div class="card-deck">
                    @foreach ($other_resps as $other_resp)
                    <div class="card col-md-4" onclick="window.location='{{ route('person.show', $other_resp) }}'">
                        <img src="{{ asset($other_resp->civility->image_path) }}" class="card-img-top mx-auto avatar"
                            alt="{{ $other_resp->civility->image_path }}">
                        <div class="card-body">
                            <h5 class="card-title text-center">
                                {{ $other_resp->first_name }} <b>{{ $other_resp->last_name }}</b>
                            </h5>
                            <p class="card-text text-center">

                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div>
                <h2>MON TABLEAU DE BORD</h2>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="far fa-file-alt fa-2x"></i></h5>
                            <p class="card-text">
                                Dernières de demandes
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="far fa-envelope-open fa-2x"></i></h5>
                            <p class="card-text">
                                Echanges à lire
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-file-invoice-dollar fa-2x"></i></h5>
                            <p class="card-text">
                                Mes factures
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-file-archive fa-2x"></i></i></h5>
                            <p class="card-text">
                                Pièces justificatives
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-map-marker-alt fa-2x"></i></i></i></h5>
                            <p class="card-text">
                                Coordonnées
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h2>MA FAMILLE</h2>
                <div class="card-deck">
                    <div class="card" onclick="window.location='{{ route('registration.index') }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="far fa-clock fa-2x"></i></h5>
                            <p class="card-text">
                                Modifier/Annuler les...
                            </p>
                        </div>
                    </div>
                    <div class="card" onclick="window.location='{{ route('registration.create') }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-file-signature fa-2x"></i></h5>
                            <p class="card-text">
                                Créer une inscription
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-calculator fa-2x"></i></h5>
                            <p class="card-text">
                                Simulation des tarifs
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection