@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <h1 class="text-center">Tabeau de bord</h1>
    <div class="row">
        <div class="col-md-4">
            <div class="card col-md-8">
                <div class="card-body text-center">
                    <h5 class="card-title">
                        MON ESPACE
                    </h5>
                    <p class="card-text d-inline-block text-left">
                        Agent {{ $user->name }}
                    </p>
                    <div class="card-footer">
                        <a href="{{ route('user.show', $user->id)}}">
                            <i class="fas fa-lock">
                                <p class="icon-profil">Identifiants</p>
                            </i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-user fa-2x"></i></h5>
                            <p class="card-text">Ajouter une personne
                            </p>
                        </div>
                    </div>
                    <div class="card" onclick="window.location='{{ route('person.index') }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-users fa-2x"></i></h5>
                            <p class="card-text">Liste des personnes
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"></h5>
                            <p class="card-text"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-plus"></i><i
                                    class="fas fa-file-signature fa-2x"></i>
                            </h5>
                            <p class="card-text">Ajouter une inscription
                            </p>
                        </div>
                    </div>
                    <div class="card" onclick="window.location='{{ route('registration.index') }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-file-signature fa-2x"></i></h5>
                            <p class="card-text">Liste des inscriptions
                            </p>
                        </div>
                    </div>
                    <div class="card" onclick="window.location='{{ route('registration.index', ['status' =>  'attente']) }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="far fa-clock fa-2x"></i></h5>
                            <p class="card-text">Liste des inscriptions en attente
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="card-deck">
                    <div class="card" onclick="window.location='{{ route('school.index') }}'">
                        <div class="card-body text-center">
                            <h5 class="card-title"><i class="fas fa-school fa-2x"></i></h5>
                            <p class="card-text">Liste des écoles
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection