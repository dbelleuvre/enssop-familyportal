@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Identifiant utilisateur</div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Nom</label>
                        <div class="col-md-6">
                            <input class="form-control" value="{{ $user->name }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Adresse mail</label>

                        <div class="col-md-6">
                            <input class="form-control" value="{{$user->email }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Mot de Passe</label>

                        <div class="col-md-6">
                            <input type="password" class="form-control" value="{{ $user->password }}" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection