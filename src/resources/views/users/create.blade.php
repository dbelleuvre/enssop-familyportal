@extends('FamilyPortal::layouts.master')

@section('content')
<div class="container">
    <div class="container">
        <form class="my-5" action="{{ route('person.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-4">
                    <h4 class="my-5 text-center">Identité</h4>
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><b>Civilité :</b></label>
                        <div class="col-9">
                            @foreach ($civilities as $civility)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="civility_id"
                                    id="civility_other_{{ $civility->id }}" value="{{ $civility->id }}">
                                <label class="form-check-label" for="civility_other_{{ $civility->id }}">
                                    @if ( $civility->short_name == null )
                                    {{ $civility->title }}
                                    @else
                                    {{ $civility->short_name }}
                                    @endif
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="last_name" class="col-3 col-form-label"><b>Nom :</b></label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="last_name" name="last_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="first_name" class="col-3 col-form-label"><b>Prénom :</b></label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="first_name" name="first_name">
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <h4 class="my-5 text-center">Coordonnées</h4>
                    <div class="form-group row">
                        <label for="adress" class="col-3 col-form-label">Adresse :</label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="adress" name="adress">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="postalcode" class="col-3 col-form-label">Code Postal :</label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="postalcode" name="postalcode">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-3 col-form-label">Ville :</label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="city" name="city">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-3 col-form-label">Téléphone Domicile :</label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="phone" name="phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mobile_phone" class="col-3 col-form-label">Téléphone Portable :</label>
                        <div class="col-9">
                            <input class="form-control" type="text" id="mobile_phone" name="mobile_phone">
                        </div>
                    </div>
                </div>
                <div class="mx-auto">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection